import Link from 'next/link'
import {Navbar,Nav,NavDropdown} from 'react-bootstrap'

export default function NavBar () {


	return (


		        <Navbar variant="light" expand="lg" className="mb-md-5 ">
		            <Link href="/">
		                <a className="navbar-brand d-lg-none">Pilay na Pato</a>
		            </Link>
		            <Navbar.Toggle aria-controls="basic-navbar-nav" />
		            <Navbar.Collapse id="basic-navbar-nav">
		                <Nav className="mx-auto text-center">

		                            <Link href="/items">
		                                <a className="nav-link mr-lg-auto" role="button">Gallery</a>
		                            </Link>
		                            <Link href="/items">
		                                <a className="nav-link mx-lg-auto" role="button">Music</a>
		                            </Link>
		                            <Link href="/items">
		                                <a className="nav-link mr-lg-auto" role="button">Events</a>
		                            </Link>
		                            <Link href="/about">
		                                <a className="nav-link ml-lg-auto" role="button">About</a>
		                            </Link>
		                            <Link href="/items">
		                                <a className="nav-link mr-lg-auto" role="button">Contact</a>
		                            </Link>


		                </Nav>
		            </Navbar.Collapse>
		        </Navbar>


		)



}