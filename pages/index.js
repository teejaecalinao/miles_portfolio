import {useState, useEffect} from 'react'
import Head from 'next/head'
import {Container,Row,Col,Jumbotron} from 'react-bootstrap'
import { FaFacebookSquare, FaInstagramSquare, FaSoundcloud, FaYoutubeSquare } from "react-icons/fa";

export default function Home() {

  return (

    <Container>
    <Row id="#" className="mt-5 mt-md-5 mt-lg-2">
      <Col md="3" className="mx-auto mt-5 mt-md-5 mt-lg-2">
        <img src="/images/chibi.png" className="img-fluid" alt=""/>
      </Col>
    </Row>
    <Row>
      <Col md="6" className="mx-auto">
          <h1 className="display-1 text-center d-none d-lg-block">Pilay Na Pato</h1>
          <h1 className="display-4 text-center d-none d-md-block d-lg-none">Pilay Na Pato</h1>
          <h1 className="text-center d-block d-md-none">Pilay Na Pato</h1>
      </Col>
    </Row>
    <Row>
      <div className="mt-2 mx-auto col-10 col-md-5 col-lg-3">
        <span className="mr-1"> <FaFacebookSquare size={40} /></span>
        <span className="ml-3 mr-2"> <FaInstagramSquare size={40} /></span>
        <span className="ml-2 mr-3"> <FaYoutubeSquare size={40} /></span>
        <span className="ml-1"> <FaSoundcloud size={40} /></span>
      </div>       
    </Row>
</Container>

  )
}
